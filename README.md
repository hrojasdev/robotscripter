# RobotScripter

Simple robot scripter.<br>
By Hernan Rojas.<br>
Last edited: February 13th, 2019.<br>

# What it does

This is a simple program that allows controlling toy robots that move on a table, through commands introduced via scripting or by standard input.<br>

Commands supported:<br>

 * PLACE X,Y,F: locates the robot on the table in position (X, Y), facing either NORTH, SOUTH, EAST or WEST.
 * MOVE: moves the robot 1 unit forward in the direction it is facing.
 * LEFT: rotate the robot 90 degrees to the left without changing its position.
 * RIGHT: rotate the robot 90 degrees to the right without changing its position.
 * REPORT: prints the current position of the robot.

# Constraints

 * Table limited to 5 x 5 positions.
 * The origin (0,0) is considered the SOUTH-WEST most corner.
 * Robot must move freely, there are no obstructions on the table.
 * The robot must be prevented from falling off the table. Further movement must be allowed.
 * The robot must not be placed in a position outside the table.
 * The first command accepted by the robot is PLACE. Commands before a valid PLACE command must be discarded.
 * A robot that has not been placed on the table ignores the commands MOVE, LEFT, RIGHT and REPORT.
 * No graphical output showing the movement of the robot required.

# Solution description

My aim was to deliver an extensible and clean solution. At its core, it relies on the Command pattern which, by encapsulating in an object all the information necessary to perform an action, makes it easy to add support for new commands and types of robots. I also relied on TDD to make the code as robust as possible. A complete implementation of a robot is provided.<br>

Delving into this implementation of the Command pattern, the provided classes that map to those of the pattern are:
 * Receiver: TableRobot interface and sample implementation, Rover.
 * Command: Command interface and 5 implementations: PlaceRoverCommand, MoveRoverCommand, LeftRoverCommand, RightRoverCommand, ReportRoverCommand.
 * Invoker: RobotInvoker.
 * Client: RobotScript, the main application.

The application provides 2 UI modes:
 * Script execution: activated when a file is passed as a command line argument. The application executes the provided script, displays results and exits.
 * Interactive: if no script file was provided, the application enters interactive mode, where the user can enter commands. A HELP command is provided to display the available commands as well as EXIT, to quit the application. 

## Other design decisions

 * Extensibility in this project focuses on new commands and robots but not on new surfaces, as it was not a requirement and helps keep the code simple. Robots know all valid positions they can move to.
 * New robots must implement the TableRobot interface. Robots that move in different spaces are not supported.
 * There are some methods with default (package) access, only to make them available to tests.
provided to exit the application. 
 * The entry point class, RobotScripter, has all code supporting the 2 modes. Did not attempt to extract the execution modes logic from the main application class, as the Command-pattern Client, it should hold the invokers, commands and receivers.

## How to build and run

Java 8 and Apache Maven are needed to build and run the tests. To achieve this, launch the command line and run: 
```
> mvn clean install
```

After all tests have passed and the executable jar file is built (./target/robotscripter-0.0.1-SNAPSHOT-jar-with-dependencies.jar), launch the application by typing:
```
> java -jar robotscripter-0.0.1-SNAPSHOT-jar-with-dependencies.jar example1.txt
```

Launching the application directly from maven is also supported:
```
> mvn exec:java -Dexec.args="example1.txt" -quiet
```

Please, find some sample script files in the test resources folder (src/test/resources).<br>

Thanks for reading this file!
