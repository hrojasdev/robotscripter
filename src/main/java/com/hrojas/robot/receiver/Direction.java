package com.hrojas.robot.receiver;

public enum Direction {

	NORTH, EAST, SOUTH, WEST;

	public Direction getRight()
	{
		return values()[(ordinal() + 1) % 4];
	}

	public Direction getLeft()
	{
		return values()[(ordinal() + 3) % 4];
	}
}
