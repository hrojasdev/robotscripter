package com.hrojas.robot.receiver;

import com.hrojas.robot.receiver.exception.InvalidPositionException;

/**
 * A position for {@link TableRobot}. 
 */
public class TablePosition
{
	public static Integer X_MAX_POSITION = 4;
	public static Integer Y_MAX_POSITION = 4;

	private Integer x;
	private Integer y;
	private Direction f;

	/**
	 * Returns a new {@link TablePosition} with the supplied parameters.
	 * 
	 * @param xPos
	 * @param yPos
	 * @param fPos
	 * @throws InvalidPositionException
	 */
	public TablePosition(Integer xPos, Integer yPos, Direction fPos) throws InvalidPositionException
	{
		x = xPos;
		y = yPos;
		f = fPos;
	}

	/**
	 * Set a new x position.
	 * 
	 * @param xPos
	 */
	public void setX(Integer xPos)
	{
		x = xPos;
	}

	/**
	 * @return the x position
	 */
	public Integer getX()
	{
		return x;
	}

	/**
	 * Set a new y position.
	 * 
	 * @param yPos
	 */
	public void setY(Integer yPos)
	{
		y = yPos;
	}

	/**
	 * @return the y position
	 */
	public Integer getY()
	{
		return y;
	}

	/**
	 * Set a new facing direction.
	 * 
	 * @param fPos
	 */
	public void setF(Direction fPos)
	{
		f = fPos;
	}

	/**
	 * @return the direction the robot faces.
	 */
	public Direction getF()
	{
		return f;
	}

	/**
	 * @return a copy of this object.
	 */
	public TablePosition clone()
	{
		return new TablePosition(x, y, f);
	}

	/**
	 * @return whether this is a valid position.
	 */
	public boolean isValid()
	{
		return isXPositionValid(x) && isYPositionValid(y) && isDirectionValid(f);
	}

	private boolean isXPositionValid(Integer xPos)
	{
		return xPos != null && xPos >= 0 && xPos <= X_MAX_POSITION;
	}

	private boolean isYPositionValid(Integer yPos)
	{
		return yPos != null && yPos >= 0 && yPos <= Y_MAX_POSITION;
	}

	private boolean isDirectionValid(Direction direction)
	{
		return direction != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return String.format("(%s, %s) facing %s.", x, y, f);
	}
}
