package com.hrojas.robot.receiver.exception;

/**
 * Exception for when a Robot is commanded to move without having been placed.
 */
public class RobotNotPlacedException extends IllegalStateException
{
	private static final long serialVersionUID = 4244835950831402829L;

    public RobotNotPlacedException(String message) {
        super(message);
    }
}
