package com.hrojas.robot.receiver.exception;

/**
 * Exception for invalid positions
 */
public class InvalidPositionException extends IllegalArgumentException
{
	private static final long serialVersionUID = 4244835950831402829L;

    public InvalidPositionException(String message) {
        super(message);
    }
}
