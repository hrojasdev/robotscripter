package com.hrojas.robot.receiver.rover;

import com.hrojas.robot.receiver.TablePosition;
import com.hrojas.robot.receiver.TableRobot;
import com.hrojas.robot.receiver.exception.InvalidPositionException;
import com.hrojas.robot.receiver.exception.RobotNotPlacedException;

/**
 * Mars Rover implementation of {@link TableRobot}.
 */
public class Rover implements TableRobot
{
	private TablePosition position;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void place(TablePosition aPosition) throws InvalidPositionException
	{
		if (aPosition.isValid())
		{
			position = aPosition;
		}
		else
		{
			throw new InvalidPositionException(String.format("Invalid position %s.", aPosition));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void move() throws RobotNotPlacedException, InvalidPositionException
	{
		validateCurrentPosition();

		TablePosition target = position.clone();
		
		switch (position.getF())
		{
			case NORTH:
				target.setY(target.getY() + 1);
				break;
			case EAST:
				target.setX(target.getX() + 1);
				break;
			case SOUTH:
				target.setY(target.getY() - 1);
				break;
			case WEST:
				target.setX(target.getX() - 1);
				break;
		}

		place(target);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void right() throws RobotNotPlacedException
	{
		validateCurrentPosition();

		TablePosition target = position.clone();
		target.setF(target.getF().getRight());
		place(target);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void left() throws RobotNotPlacedException
	{
		validateCurrentPosition();

		TablePosition target = position.clone();
		target.setF(target.getF().getLeft());
		place(target);
	}

	private void validateCurrentPosition()
	{
		if (position == null)
		{
			throw new RobotNotPlacedException("Rover has not been correctly placed.");
		}
	}

	TablePosition getPosition()
	{
		return position;
	}

	@Override
	public void report() throws RobotNotPlacedException 
	{
		validateCurrentPosition();

		System.out.println(position.toString());
	}
}
