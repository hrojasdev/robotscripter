package com.hrojas.robot.receiver;

import com.hrojas.robot.receiver.exception.InvalidPositionException;
import com.hrojas.robot.receiver.exception.RobotNotPlacedException;

/**
 * A robot that moves on a table without falling off of it.
 */
public interface TableRobot
{
	/**
	 * Places the robot in a given {@link TablePosition}.
	 * 
	 * @param aPosition
	 * @throws InvalidPositionException
	 */
	void place(TablePosition aPosition) throws InvalidPositionException;

	/**
	 * Moves the robot one position to the front.
	 * 
	 * @throws RobotNotPlacedException if a position has not been set to the robot
	 *                                 through {@link #place(TablePosition)}.
	 */
	void move() throws RobotNotPlacedException, InvalidPositionException;

	/**
	 * Turns the robot to face right of its current position.
	 * 
	 * @throws RobotNotPlacedException if a position has not been set to the robot
	 *                                 through {@link #place(TablePosition)}.
	 */
	void right() throws RobotNotPlacedException;

	/**
	 * Turns the robot to face left of its current position.
	 * 
	 * @throws RobotNotPlacedException if a position has not been set to the robot
	 *                                 through {@link #place(TablePosition)}.
	 */
	void left() throws RobotNotPlacedException;

	/**
	 * Announces the current position of the robot.
	 * 
	 * @throws RobotNotPlacedException if a position has not been set to the robot
	 *                                 through {@link #place(TablePosition)}.
	 */
	void report() throws RobotNotPlacedException;
}
