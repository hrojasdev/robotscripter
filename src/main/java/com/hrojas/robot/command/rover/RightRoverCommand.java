package com.hrojas.robot.command.rover;

import com.hrojas.robot.command.Command;
import com.hrojas.robot.receiver.exception.RobotNotPlacedException;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Turns a {@link Rover} to the right.
 */
public class RightRoverCommand implements Command {

	private Rover rover;
	
	public RightRoverCommand(Rover theRover)
	{
		rover = theRover;
	}
	
	public void execute()
	{
		if (rover != null)
		{
			try
			{
				rover.right();
			} catch (RobotNotPlacedException e)
			{
				// ignoring, as per project constraints.
			}
		}
	}
}
