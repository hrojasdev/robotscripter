package com.hrojas.robot.command.rover;

import com.hrojas.robot.command.Command;
import com.hrojas.robot.receiver.exception.InvalidPositionException;
import com.hrojas.robot.receiver.exception.RobotNotPlacedException;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Moves a {@link Rover} one position to the front.
 */
public class MoveRoverCommand implements Command {

	private Rover rover;
	
	public MoveRoverCommand(Rover theRover)
	{
		rover = theRover;
	}
	
	public void execute()
	{
		if (rover != null)
		{
			try
			{
				rover.move();
			} catch (RobotNotPlacedException e)
			{
				// must not move if not placed.
				// ignoring, as per project constraints.
			}
			catch (InvalidPositionException e)
			{
				// must not fall off the table.
				// ignoring, as per project constraints.
			}
		}
	}
}
