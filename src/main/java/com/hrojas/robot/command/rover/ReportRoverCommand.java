package com.hrojas.robot.command.rover;

import com.hrojas.robot.command.Command;
import com.hrojas.robot.receiver.exception.RobotNotPlacedException;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Announces a Rover's current position.
 */
public class ReportRoverCommand implements Command {

	private Rover rover;
	
	public ReportRoverCommand(Rover theRover)
	{
		rover = theRover;
	}
	
	public void execute()
	{
		if (rover != null)
		{
			try
			{
				rover.report();
			} catch (RobotNotPlacedException e)
			{
				// ignoring, as per project constraints.
			}
		}
	}
}
