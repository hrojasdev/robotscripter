package com.hrojas.robot.command.rover;

import com.hrojas.robot.command.Command;
import com.hrojas.robot.receiver.TablePosition;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Places a Rover in a supplied position.
 */
public class PlaceRoverCommand implements Command {

	private Rover rover;
	
	private TablePosition position;

	public PlaceRoverCommand(Rover theRover, TablePosition aPosition)
	{
		rover = theRover;
		position = aPosition;
	}
	
	public void execute()
	{
		if (rover != null && position != null && position.isValid())
		{
			rover.place(position);
		}
	}
}
