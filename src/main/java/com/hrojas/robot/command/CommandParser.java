package com.hrojas.robot.command;

import static java.util.Optional.ofNullable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.hrojas.robot.command.exception.CommandFormatException;
import com.hrojas.robot.command.rover.LeftRoverCommand;
import com.hrojas.robot.command.rover.MoveRoverCommand;
import com.hrojas.robot.command.rover.PlaceRoverCommand;
import com.hrojas.robot.command.rover.ReportRoverCommand;
import com.hrojas.robot.command.rover.RightRoverCommand;
import com.hrojas.robot.receiver.Direction;
import com.hrojas.robot.receiver.TablePosition;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Parses strings into robot {@link Command}s.
 */
public class CommandParser
{
	public enum CommandExpression
	{
		PLACE("\\s*(\\d+)\\s*,\\s*(\\d+)\\s*,\\s*(\\b" + enumValuesForMatching(Direction.class) + "\\b)\\s*$"),
		MOVE(null),
		RIGHT(null),
		LEFT(null),
		REPORT(null);

		private static String DEFAULT_PATTERN = ".*(\\b%s\\b)%s";

		private String paramsPattern;

		private CommandExpression(String aPattern) {
			paramsPattern = aPattern;
		}

		/**
		 * @return compiled regular expression that matches this {@link CommandExpression}.
		 */
		public Pattern getPattern()
		{
			return Pattern.compile(String.format(DEFAULT_PATTERN, name(), ofNullable(paramsPattern).orElse("")));
		}
	}

	/**
	 * Parses the supplied string into its corresponding {@link Command}, passing
	 * the received {@link Rover} as parameter.
	 * 
	 * @param input a string
	 * @param robot required by all {@link Command}s.
	 * @return a valid {@link Command} corresponding to the supplied expression
	 * @throws CommandFormatException if the supplied expression is null, empty,
	 *                                unrecognised or is in an invalid format
	 */
	public static Command parseCommand(String input, Rover robot) throws CommandFormatException
	{
		if (input == null || input.isEmpty())
		{
			throw new CommandFormatException(input);
		}

		CommandExpression exp = Stream.of(CommandExpression.values())
				.filter(ce -> ce.getPattern().matcher(input).matches())
				.findFirst()
				.orElseThrow(() -> new CommandFormatException("Unrecognised command: " + input));

		switch (exp)
		{
			case PLACE:
				Matcher matcher = exp.getPattern().matcher(input);

				while (matcher.find())
				{
					Integer xPos = Integer.parseInt(matcher.group(2));
					Integer yPos = Integer.parseInt(matcher.group(3));
					Direction fPos = Direction.valueOf(matcher.group(4));

					return new PlaceRoverCommand(robot, new TablePosition(xPos, yPos, fPos));
				}
			case MOVE:
				return new MoveRoverCommand(robot);
			case RIGHT:
				return new RightRoverCommand(robot);
			case LEFT:
				return new LeftRoverCommand(robot);
			case REPORT:
				return new ReportRoverCommand(robot);
		}

		throw new CommandFormatException("Unrecognised command: " + input);
	}

	private static <T extends Enum<T>> String enumValuesForMatching(Class<T> enumClass)
	{
		return Stream.of(enumClass.getEnumConstants()).map(exp -> exp.name()).collect(Collectors.joining("|"));
	}
	
}
