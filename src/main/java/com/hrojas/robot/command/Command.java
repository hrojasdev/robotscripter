package com.hrojas.robot.command;

/**
 * Interface for command decoupling.
 */
@FunctionalInterface
public interface Command
{
	/**
	 * Perform the action.
	 */
	void execute();
}
