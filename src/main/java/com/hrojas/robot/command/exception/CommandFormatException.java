package com.hrojas.robot.command.exception;

/**
 * Thrown to indicate that the application has attempted to convert a string in
 * invalid format to a {@link Command}.
 */
public class CommandFormatException extends IllegalArgumentException
{
	private static final long serialVersionUID = 5291255291342844641L;

	public CommandFormatException(String message) {
        super(message);
    }
}
