package com.hrojas.robot.invoker;

import static java.util.Optional.ofNullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hrojas.robot.command.Command;

/**
 * Executes ordered commands on a robot and keeps track of their execution.
 */
public class RobotInvoker
{
	private List<Command> history = new ArrayList<Command>();

	public void storeAndExecute(Command command)
	{
		ofNullable(command).ifPresent(cmd -> {
			history.add(command);
			command.execute();
		});
	}

	public List<Command> getHistory()
	{
		return Collections.unmodifiableList(history);
	}
}
