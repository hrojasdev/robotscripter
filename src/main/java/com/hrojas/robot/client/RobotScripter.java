package com.hrojas.robot.client;

import static java.util.Optional.ofNullable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.hrojas.robot.command.Command;
import com.hrojas.robot.command.CommandParser;
import com.hrojas.robot.command.exception.CommandFormatException;
import com.hrojas.robot.invoker.RobotInvoker;
import com.hrojas.robot.receiver.TableRobot;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Main entry point for the RobotScripter application.
 */
public class RobotScripter<T extends TableRobot>
{
	private T robot;
	private RobotInvoker invoker;

	public RobotScripter(T aRobot, RobotInvoker anInvoker)
	{
		robot = aRobot;
		invoker = anInvoker;
	}

	public static void main(String[] args)
	{
		greet();

		RobotScripter<Rover> scripter = new RobotScripter<Rover>(new Rover(), new RobotInvoker());

		File script = getInputFile(args);

		if (script != null)
		{
			scripter.executeScript(script);
		}
		else
		{
			scripter.enterInteractiveMode();
		}
	}

	private static File getInputFile(String[] args)
	{
		return Stream.of(args)
				.findFirst()
				.filter(filename -> !filename.isEmpty())
				.map(File::new)
				.filter(f -> f.isFile())
				.orElse(null);
	}

	/*
	 * "Script" execution mode.
	 */
	void executeScript(File script)
	{
		System.out.println(String.format("> Loading file %s...\n", script.getAbsolutePath()));

		List<Command> commands = new ArrayList<Command>();

		try (Scanner scanner = new Scanner(script))
		{
			while (scanner.hasNextLine())
			{
				ofNullable(parseCommand(scanner.nextLine(), robot))
						.ifPresent(command -> commands.add(command));
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		commands.forEach(cmd -> invoker.storeAndExecute(cmd));
	}

	private static Pattern HELP_PATTERN = Pattern.compile("(\\bHELP\\b)");
	private static Pattern EXIT_PATTERN = Pattern.compile("(\\bEXIT\\b)");

	/*
	 * Start "Interactive" execution mode.
	 */
	void enterInteractiveMode()
	{
		printInteractiveModeHelp();

		try (Scanner input = new Scanner(System.in))
		{
		    while (input.hasNextLine()) {
		    	final String line = input.nextLine();

		    	if (HELP_PATTERN.matcher(line).matches())
		    	{
		    		printHelp();
		    	}
		    	else if (EXIT_PATTERN.matcher(line).matches())
		    	{
		    		return;
		    	}
		    	else
		    	{
					ofNullable(parseCommand(line, robot))
						.ifPresent(command -> invoker.storeAndExecute(command));
		    	}
		    }
		}
		catch (Exception e)
		{
			System.out.println(String.format("An error has occurred: %s. Exiting.", e.getMessage()));
		}
	}

	/*
	 * Attempts parsing an expression into a Command, ignoring errors.
	 */
	private static <T extends TableRobot> Command parseCommand(String expression, T aRobot)
	{
		try
		{
			if (aRobot instanceof Rover)
			{
				return CommandParser.parseCommand(expression, (Rover) aRobot);
			}
			else
			{
				// Other robot types not supported yet.
				return null;
			}
		}
		catch (CommandFormatException e)
		{
			// ignoring, as per project constraints
			return null;
		}
	}

	private static void greet()
	{
		System.out.println("RobotScripter v0.1\n");
	}

	private void printInteractiveModeHelp() {
		System.out.println("> No file provided. Entering interactive mode...");
		System.out.println("> Type HELP to list the supported commands or EXIT to quit the application.\n");
	}

	private void printHelp() {
		System.out.println("> Supported commands:");
		System.out.println("> PLACE X, Y, F : Place the robot in position X, Y, facing either NORTH, SOUTH, EAST or WEST");
		System.out.println("> MOVE :          Move one position ahead");
		System.out.println("> LEFT :          Turn left");
		System.out.println("> RIGHT :         Turn right");
		System.out.println("> REPORT :        Display current position\n");
	}
}
