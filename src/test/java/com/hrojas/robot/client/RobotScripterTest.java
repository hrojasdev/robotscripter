package com.hrojas.robot.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.hrojas.robot.invoker.RobotInvoker;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Tests the functionalities provided by {@link RobotScripter}.
 */
public class RobotScripterTest
{
	private static final String NEWLINE_REGEX = "\\r?\\n";

	// subject of tests
	private RobotScripter<Rover> scripter;

	private PrintStream originalOut;

	private ByteArrayOutputStream testOut;

	@BeforeEach
	public void setup()
	{
		scripter = new RobotScripter<Rover>(new Rover(), new RobotInvoker());

		originalOut = System.out;
		testOut = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOut));
	}

	@AfterEach
	public void teardown()
	{
		try
		{
			testOut.close();
		} catch (IOException e)
		{
			// ignore.
		}

		System.setOut(originalOut);
	}

	@Test
	public void should_invoke_ansi_format_script_with_all_valid_commands() throws Exception
	{
		Path path = Paths.get(getClass().getClassLoader().getResource("script-ansi.txt").toURI());

		scripter.executeScript(path.toFile());

		String lines[] = testOut.toString().split(NEWLINE_REGEX);

		assertEquals(6, lines.length);
		assertTrue(lines[2].contains("(2, 3) facing WEST"));
		assertTrue(lines[3].contains("(2, 4) facing NORTH"));
		assertTrue(lines[4].contains("(1, 4) facing WEST"));
		assertTrue(lines[5].contains("(3, 2) facing EAST"));
	}

	@Test
	public void should_invoke_utf8_format_script_with_all_valid_commands() throws Exception
	{
		Path path = Paths.get(getClass().getClassLoader().getResource("script-utf8.txt").toURI());

		scripter.executeScript(path.toFile());

		String lines[] = testOut.toString().split(NEWLINE_REGEX);

		assertEquals(6, lines.length);
		assertTrue(lines[2].contains("(2, 3) facing WEST"));
		assertTrue(lines[3].contains("(2, 4) facing NORTH"));
		assertTrue(lines[4].contains("(1, 4) facing WEST"));
		assertTrue(lines[5].contains("(3, 2) facing EAST"));
	}

	@Test
	public void should_run_script_with_all_invalid_commands_ignoring_all() throws Exception
	{
		Path path = Paths.get(getClass().getClassLoader().getResource("script-all-invalid.txt").toURI());

		scripter.executeScript(path.toFile());

		String lines[] = testOut.toString().split(NEWLINE_REGEX);

		assertEquals(1, lines.length);
	}

	@Test
	public void should_run_script_with_some_invalid_commands_ignoring_those() throws Exception
	{
		Path path = Paths.get(getClass().getClassLoader().getResource("script-some-invalid.txt").toURI());

		scripter.executeScript(path.toFile());

		String lines[] = testOut.toString().split(NEWLINE_REGEX);

		assertEquals(4, lines.length);
		assertTrue(lines[2].contains("(0, 3) facing WEST"));
		assertTrue(lines[3].contains("(1, 3) facing EAST"));
	}

	@Test
	public void should_pass_acceptance_test_1() throws Exception
	{
		Path path = Paths.get(getClass().getClassLoader().getResource("example1.txt").toURI());

		scripter.executeScript(path.toFile());

		String lines[] = testOut.toString().split(NEWLINE_REGEX);

		assertEquals(3, lines.length);
		assertTrue(lines[2].contains("(0, 1) facing NORTH"));
	}

	@Test
	public void should_pass_acceptance_test_2() throws Exception
	{
		Path path = Paths.get(getClass().getClassLoader().getResource("example2.txt").toURI());

		scripter.executeScript(path.toFile());

		String lines[] = testOut.toString().split(NEWLINE_REGEX);

		assertEquals(3, lines.length);
		assertTrue(lines[2].contains("(0, 0) facing WEST"));
	}

	@Test
	public void should_pass_acceptance_test_3() throws Exception
	{
		Path path = Paths.get(getClass().getClassLoader().getResource("example3.txt").toURI());

		scripter.executeScript(path.toFile());

		String lines[] = testOut.toString().split(NEWLINE_REGEX);

		assertEquals(3, lines.length);
		assertTrue(lines[2].contains("(3, 3) facing NORTH"));
	}

}
