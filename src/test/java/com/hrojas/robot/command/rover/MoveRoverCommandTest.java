package com.hrojas.robot.command.rover;

import static com.hrojas.robot.receiver.Direction.NORTH;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import com.hrojas.robot.receiver.TablePosition;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Tests the functionality provided by {@link MoveRoverCommand}.
 */
public class MoveRoverCommandTest
{
	// subject of tests
	MoveRoverCommand command;

	@Test
	public void should_ignore_exception_on_move_command_when_robot_not_placed()
	{
		command = new MoveRoverCommand(new Rover());

		Executable executeCommand = () -> command.execute();

		Assertions.assertDoesNotThrow(executeCommand,
				"Should ignore RobotNotPlacedException when attempting to move Robot if not placed yet.");
	}

	@Test
	public void should_move_robot_if_placed()
	{
		TablePosition position = mock(TablePosition.class);
		when(position.isValid()).thenReturn(true);

		Rover rover = new Rover();
		rover.place(position);

		Rover roverSpy = spy(rover);
		doNothing().when(roverSpy).move();

		command = new MoveRoverCommand(roverSpy);
		command.execute();

		verify(roverSpy, times(1)).move();
	}

	@Test
	public void should_ignore_exception_on_move_command_if_robot_would_fall_off_the_table()
	{
		Rover rover = new Rover();
		rover.place(new TablePosition(4, 4, NORTH));

		command = new MoveRoverCommand(rover);

		Executable executeCommand = () -> command.execute();

		Assertions.assertDoesNotThrow(executeCommand,
				"Should ignore InvalidPositionException when attempting to move Robot to a position outside the table.");
	}
}
