package com.hrojas.robot.command.rover;

import static com.hrojas.robot.receiver.Direction.NORTH;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;

import com.hrojas.robot.receiver.TablePosition;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Tests the functionality provided by {@link PlaceRoverCommand}.
 */
public class PlaceRoverCommandTest
{
	// subject of tests
	PlaceRoverCommand command;

	@Test
	public void should_not_execute_if_null_position()
	{
		Rover roverMock = mock(Rover.class);

		command = new PlaceRoverCommand(roverMock, null);
		command.execute();

		verify(roverMock, never()).place(Mockito.any());
	}

	@Test
	public void should_execute_and_ignore_if_invalid_position()
	{
		Rover rover = new Rover();
		Rover roverSpy = spy(rover);

		TablePosition posMock = mock(TablePosition.class);
		Mockito.when(posMock.isValid()).thenReturn(false);

		command = new PlaceRoverCommand(roverSpy, posMock);
		command.execute();

		verify(roverSpy, never()).place(Mockito.any());

		// the check for TablePosition.isValid() removes the possibility of InvalidPositionExceptions.
		// Not creating a unit test for this scenario.
	}

	@Test
	public void should_execute_if_valid_parameters()
	{
		Rover rover = new Rover();
		Rover roverSpy = spy(rover);

		TablePosition posMock = mock(TablePosition.class);
		Mockito.when(posMock.isValid()).thenReturn(true);

		command = new PlaceRoverCommand(roverSpy, posMock);
		command.execute();

		verify(roverSpy, times(1)).place(posMock);
	}

	@Test
	public void should_ignore_command_to_place_the_robot_outside_the_table()
	{
		command = new PlaceRoverCommand(new Rover(), new TablePosition(5, 5, NORTH));

		Executable executeCommand = () -> command.execute();

		Assertions.assertDoesNotThrow(executeCommand,
				"Should ignore InvalidPositionException when attempting to place the Robot outside the table.");
	}

}
