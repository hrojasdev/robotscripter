package com.hrojas.robot.command.rover;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import com.hrojas.robot.receiver.TablePosition;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Tests the functionality provided by {@link RightRoverCommand}.
 */
public class RightRoverCommandTest
{
	// subject of tests
	RightRoverCommand command;

	@Test
	public void should_ignore_exception_on_right_command_when_robot_not_placed()
	{
		command = new RightRoverCommand(new Rover());

		Executable executeCommand = () -> command.execute();

		Assertions.assertDoesNotThrow(executeCommand,
				"Should ignore RobotNotPlacedException when attempting to turn Robot to the right if not placed yet.");
	}

	@Test
	public void should_turn_robot_right_if_placed()
	{
		TablePosition position = mock(TablePosition.class);
		when(position.isValid()).thenReturn(true);

		Rover rover = new Rover();
		rover.place(position);

		Rover roverSpy = spy(rover);
		doNothing().when(roverSpy).right();

		command = new RightRoverCommand(roverSpy);
		command.execute();

		verify(roverSpy, times(1)).right();
	}

}
