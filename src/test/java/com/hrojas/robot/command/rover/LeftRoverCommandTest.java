package com.hrojas.robot.command.rover;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import com.hrojas.robot.receiver.TablePosition;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Tests the functionality provided by {@link LeftRoverCommand}.
 */
public class LeftRoverCommandTest
{
	// subject of tests
	LeftRoverCommand command;

	@Test
	public void should_ignore_exception_on_left_command_when_robot_not_placed()
	{
		command = new LeftRoverCommand(new Rover());

		Executable executeCommand = () -> command.execute();

		Assertions.assertDoesNotThrow(executeCommand,
				"Should ignore RobotNotPlacedException when attempting to turn Robot to the left if not placed yet.");
	}

	@Test
	public void should_turn_robot_left_if_placed()
	{
		TablePosition position = mock(TablePosition.class);
		when(position.isValid()).thenReturn(true);

		Rover rover = new Rover();
		rover.place(position);

		Rover roverSpy = spy(rover);
		doNothing().when(roverSpy).left();

		command = new LeftRoverCommand(roverSpy);
		command.execute();

		verify(roverSpy, times(1)).left();
	}

}
