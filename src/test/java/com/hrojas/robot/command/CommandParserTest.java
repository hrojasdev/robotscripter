package com.hrojas.robot.command;

import static com.hrojas.robot.command.CommandParser.parseCommand;
import static com.hrojas.robot.receiver.TablePosition.X_MAX_POSITION;
import static com.hrojas.robot.receiver.TablePosition.Y_MAX_POSITION;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.hrojas.robot.command.exception.CommandFormatException;
import com.hrojas.robot.command.rover.LeftRoverCommand;
import com.hrojas.robot.command.rover.MoveRoverCommand;
import com.hrojas.robot.command.rover.PlaceRoverCommand;
import com.hrojas.robot.command.rover.ReportRoverCommand;
import com.hrojas.robot.command.rover.RightRoverCommand;
import com.hrojas.robot.receiver.Direction;
import com.hrojas.robot.receiver.rover.Rover;

/**
 * Tests the functionalities provided by {@link CommandParser}.
 */
public class CommandParserTest
{
	private Rover rover;
	
	@BeforeEach
	public void setup()
	{
		rover = Mockito.mock(Rover.class);
	}
	
	@Test
	public void should_not_parse_null()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand(null, rover);
		});
	}

	@Test
	public void should_not_parse_empty_string()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("", rover);
		});
	}

	@Test
	public void should_not_parse_unrecognised_command()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("TEST", rover);
		});
	}

	@Test
	public void should_parse_move_rover_command()
	{
		Assertions.assertTrue(parseCommand("MOVE", rover) instanceof MoveRoverCommand);
	}

	@Test
	public void should_parse_right_rover_command()
	{
		Assertions.assertTrue(parseCommand("RIGHT", rover) instanceof RightRoverCommand);
	}

	@Test
	public void should_parse_left_rover_command()
	{
		Assertions.assertTrue(parseCommand("LEFT", rover) instanceof LeftRoverCommand);
	}

	@Test
	public void should_parse_report_rover_command()
	{
		Assertions.assertTrue(parseCommand("REPORT", rover) instanceof ReportRoverCommand);
	}

	@Test
	public void should_parse_place_rover_command_in_every_valid_position()
	{
		IntStream.rangeClosed(0, X_MAX_POSITION).forEach(x -> {
			IntStream.rangeClosed(0, Y_MAX_POSITION).forEach(y -> {
				Stream.of(Direction.values()).forEach(f -> {

					assertTrue(parseCommand(String.format("PLACE %s, %s, %s", x, y, f),
							rover) instanceof PlaceRoverCommand);

				});
			});
		});
	}

	@Test
	public void should_not_parse_place_rover_command_with_just_x()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("PLACE 4", rover);
		});
	}

	@Test
	public void should_not_parse_place_rover_command_with_just_x_and_y()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("PLACE 2, 0", rover);
		});
	}

	@Test
	public void should_not_parse_place_rover_command_with_just_f()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("PLACE NORTH", rover);
		});
	}

	@Test
	public void should_not_parse_place_rover_command_with_invalid_x()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("PLACE X, 3, WEST", rover);
		});
	}

	@Test
	public void should_not_parse_place_rover_command_with_invalid_y()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("PLACE 1, Y, SOUTH", rover);
		});
	}
	
	@Test
	public void should_not_parse_place_rover_command_with_invalid_f()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("PLACE 4, 3, FAKE", rover);
		});
	}

	@Test
	public void should_not_parse_place_rover_command_without_parameters()
	{
		Assertions.assertThrows(CommandFormatException.class, () -> {
			parseCommand("PLACE", rover);
		});
	}

}
