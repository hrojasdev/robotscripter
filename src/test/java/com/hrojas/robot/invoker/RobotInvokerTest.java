package com.hrojas.robot.invoker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.hrojas.robot.command.Command;

/**
 * Executes ordered commands on a robot and keeps track of their execution.
 */
public class RobotInvokerTest
{
	private RobotInvoker invoker;

	@BeforeEach
	public void setup()
	{
		invoker = new RobotInvoker();
	}

	@Test
	public void should_not_store_or_execute_null_commands()
	{
		invoker.storeAndExecute(null);

		assertTrue(invoker.getHistory().isEmpty());
	}

	@Test
	public void should_store_and_execute_non_null_commands()
	{
		Command command = mock(Command.class);

		invoker.storeAndExecute(command);

		assertEquals(1, invoker.getHistory().size());
		verify(command, times(1)).execute();
	}
}
