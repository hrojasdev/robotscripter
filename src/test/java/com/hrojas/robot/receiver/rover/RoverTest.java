package com.hrojas.robot.receiver.rover;

import static com.hrojas.robot.receiver.Direction.EAST;
import static com.hrojas.robot.receiver.Direction.NORTH;
import static com.hrojas.robot.receiver.Direction.SOUTH;
import static com.hrojas.robot.receiver.Direction.WEST;
import static com.hrojas.robot.receiver.TablePosition.X_MAX_POSITION;
import static com.hrojas.robot.receiver.TablePosition.Y_MAX_POSITION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.hrojas.robot.receiver.Direction;
import com.hrojas.robot.receiver.TablePosition;
import com.hrojas.robot.receiver.exception.InvalidPositionException;
import com.hrojas.robot.receiver.exception.RobotNotPlacedException;

public class RoverTest
{
	// subject of tests
	private Rover rover;

	@BeforeEach
	public void setup()
	{
		rover = new Rover();
	}

	@Test
	public void should_place_in_valid_position()
	{
		IntStream.rangeClosed(0, X_MAX_POSITION).forEach(x -> {
			IntStream.rangeClosed(0, Y_MAX_POSITION).forEach(y -> {
				Stream.of(Direction.values()).forEach(f -> {
					rover.place(new TablePosition(x, y, f));

					assertEquals(x, rover.getPosition().getX());
					assertEquals(y, rover.getPosition().getY());
					assertEquals(f, rover.getPosition().getF());
				});
			});
		});
	}

	@Test
	public void should_fail_placing_in_bigger_x_position()
	{
		assertThrows(InvalidPositionException.class, () -> {
			rover.place(new TablePosition(X_MAX_POSITION + 1, 3, NORTH));
		});
	}

	@Test
	public void should_fail_placing_in_negative_x_position()
	{
		assertThrows(InvalidPositionException.class, () -> {
			rover.place(new TablePosition(-1, 3, NORTH));
		});
	}

	@Test
	public void should_fail_placing_in_bigger_y_position()
	{
		assertThrows(InvalidPositionException.class, () -> {
			rover.place(new TablePosition(3, Y_MAX_POSITION + 1, NORTH));
		});
	}

	@Test
	public void should_fail_placing_in_negative_y_position()
	{
		assertThrows(InvalidPositionException.class, () -> {
			rover.place(new TablePosition(3, -1, NORTH));
		});
	}

	@Test
	public void should_fail_placing_in_null_x_position()
	{
		assertThrows(InvalidPositionException.class, () -> {
			rover.place(new TablePosition(null, 3, NORTH));
		});
	}

	@Test
	public void should_fail_placing_in_null_y_position()
	{
		assertThrows(InvalidPositionException.class, () -> {
			rover.place(new TablePosition(3, null, NORTH));
		});
	}

	@Test
	public void should_fail_placing_in_null_direction()
	{
		assertThrows(InvalidPositionException.class, () -> {
			rover.place(new TablePosition(3, 3, null));
		});
	}

	@Test
	public void should_not_move_if_not_placed()
	{
		assertThrows(RobotNotPlacedException.class, () -> {
			rover.move();
		});
	}

	@Test
	public void should_move_to_all_valid_positions_facing_north()
	{
		IntStream.rangeClosed(0, X_MAX_POSITION).forEach(x -> {
			IntStream.rangeClosed(0, Y_MAX_POSITION - 1).forEach(y -> {
				rover.place(new TablePosition(x, y, NORTH));
				rover.move();

				assertEquals(x, rover.getPosition().getX());
				assertEquals(y + 1, rover.getPosition().getY());
				assertEquals(NORTH, rover.getPosition().getF());
			});
		});
	}

	@Test
	public void should_move_to_all_valid_positions_facing_south()
	{
		IntStream.rangeClosed(0, X_MAX_POSITION).forEach(x -> {
			IntStream.rangeClosed(1, Y_MAX_POSITION).forEach(y -> {
				rover.place(new TablePosition(x, y, SOUTH));
				rover.move();

				assertEquals(x, rover.getPosition().getX());
				assertEquals(y - 1, rover.getPosition().getY());
				assertEquals(SOUTH, rover.getPosition().getF());
			});
		});
	}

	@Test
	public void should_move_to_all_valid_positions_facing_east()
	{
		IntStream.rangeClosed(0, X_MAX_POSITION - 1).forEach(x -> {
			IntStream.rangeClosed(0, Y_MAX_POSITION).forEach(y -> {
				rover.place(new TablePosition(x, y, EAST));
				rover.move();

				assertEquals(x + 1, rover.getPosition().getX());
				assertEquals(y, rover.getPosition().getY());
				assertEquals(EAST, rover.getPosition().getF());
			});
		});
	}

	@Test
	public void should_move_to_all_valid_positions_facing_west()
	{
		IntStream.rangeClosed(1, X_MAX_POSITION).forEach(x -> {
			IntStream.rangeClosed(0, Y_MAX_POSITION).forEach(y -> {
				rover.place(new TablePosition(x, y, WEST));
				rover.move();

				assertEquals(x - 1, rover.getPosition().getX());
				assertEquals(y, rover.getPosition().getY());
				assertEquals(WEST, rover.getPosition().getF());
			});
		});
	}

	@Test
	public void should_not_fall_from_table_on_move_facing_north()
	{
		IntStream.rangeClosed(0, X_MAX_POSITION).forEach(x -> {
			rover.place(new TablePosition(x, Y_MAX_POSITION, NORTH));

			assertThrows(InvalidPositionException.class, () -> {
				rover.move();
			});
		});
	}

	@Test
	public void should_not_fall_from_table_on_move_facing_south()
	{
		IntStream.rangeClosed(0, X_MAX_POSITION).forEach(x -> {
			rover.place(new TablePosition(x, 0, SOUTH));

			assertThrows(InvalidPositionException.class, () -> {
				rover.move();
			});
		});
	}

	@Test
	public void should_not_fall_from_table_on_move_facing_east()
	{
		IntStream.rangeClosed(0, Y_MAX_POSITION).forEach(y -> {
			rover.place(new TablePosition(X_MAX_POSITION, y, EAST));

			assertThrows(InvalidPositionException.class, () -> {
				rover.move();
			});
		});
	}

	@Test
	public void should_not_fall_from_table_on_move_facing_west()
	{
		IntStream.rangeClosed(0, Y_MAX_POSITION).forEach(y -> {
			rover.place(new TablePosition(0, y, WEST));

			assertThrows(InvalidPositionException.class, () -> {
				rover.move();
			});
		});
	}

	@Test
	public void should_not_turn_right_if_not_placed()
	{
		assertThrows(RobotNotPlacedException.class, () -> {
			rover.right();
		});
	}

	@Test
	public void should_turn_right_correctly_if_placed()
	{
		rover.place(new TablePosition(3, 3, NORTH));
		rover.right();
		assertEquals(EAST, rover.getPosition().getF());

		rover.right();
		assertEquals(SOUTH, rover.getPosition().getF());
		
		rover.right();
		assertEquals(WEST, rover.getPosition().getF());
		
		rover.right();
		assertEquals(NORTH, rover.getPosition().getF());
	}

	@Test
	public void should_not_turn_left_if_not_placed()
	{
		assertThrows(RobotNotPlacedException.class, () -> {
			rover.left();
		});
	}

	@Test
	public void should_turn_left_correctly_if_placed()
	{
		rover.place(new TablePosition(3, 3, NORTH));
		rover.left();
		assertEquals(WEST, rover.getPosition().getF());

		rover.left();
		assertEquals(SOUTH, rover.getPosition().getF());
		
		rover.left();
		assertEquals(EAST, rover.getPosition().getF());
		
		rover.left();
		assertEquals(NORTH, rover.getPosition().getF());
	}

	@Test
	public void should_not_report_if_not_placed()
	{
		assertThrows(RobotNotPlacedException.class, () -> {
			rover.report();
		});
	}

	@Test
	public void should_report_correctly_if_well_placed()
	{
		IntStream.rangeClosed(0, X_MAX_POSITION).forEach(x -> {
			IntStream.rangeClosed(0, Y_MAX_POSITION).forEach(y -> {
				Stream.of(Direction.values()).forEach(f -> {
					rover.place(new TablePosition(x, y, f));

					final PrintStream originalOut = System.out;
					final ByteArrayOutputStream output = new ByteArrayOutputStream();
					System.setOut(new PrintStream(output));

					rover.report();
					assertTrue(output.toString().contains(String.format("(%s, %s) facing %s", x, y, f)));

					System.setOut(originalOut);
				});
			});
		});
	}

}
