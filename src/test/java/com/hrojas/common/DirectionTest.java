package com.hrojas.common;

import static com.hrojas.robot.receiver.Direction.EAST;
import static com.hrojas.robot.receiver.Direction.NORTH;
import static com.hrojas.robot.receiver.Direction.SOUTH;
import static com.hrojas.robot.receiver.Direction.WEST;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DirectionTest {

	@Test
	public void testGetRight() {
		Assertions.assertEquals(EAST, NORTH.getRight());
		Assertions.assertEquals(SOUTH, EAST.getRight());
		Assertions.assertEquals(WEST, SOUTH.getRight());
		Assertions.assertEquals(NORTH, WEST.getRight());
	}

	@Test
	public void testGetLeft() {
		Assertions.assertEquals(WEST, NORTH.getLeft());
		Assertions.assertEquals(SOUTH, WEST.getLeft());
		Assertions.assertEquals(EAST, SOUTH.getLeft());
		Assertions.assertEquals(NORTH, EAST.getLeft());
	}

}
